# preferred editor
if type "code" >/dev/null; then
    export EDITOR="code"
else
    export EDITOR="nano"
fi

# ZSH Plugins
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# custom plugins
source ~/.zsh/plugins/keybinds.zsh
source ~/.zsh/plugins/git.zsh
source ~/.zsh/plugins/history.zsh
source ~/.zsh/plugins/utils.zsh
source ~/.zsh/plugins/node.zsh
source ~/.zsh/plugins/docker.zsh
