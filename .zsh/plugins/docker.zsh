# docker
alias d="docker"
alias dls="docker container ls -a"

# run an interactive container with ubuntu
alias dit="docker container run -it --rm ubuntu /bin/bash"

# docker compose
alias dc="docker compose"
alias dcu="docker compose up -d"
alias dcd="docker compose down"
