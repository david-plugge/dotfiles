if type "git" >/dev/null; then
    function grename() {
        if [[ -z "$1" || -z "$2" ]]; then
            echo "Usage: $0 old_branch new_branch"
            return 1
        fi

        # Rename branch locally
        git branch -m "$1" "$2"
        # Rename branch in origin remote
        if git push origin :"$1"; then
            git push --set-upstream origin "$2"
        fi
    }

    alias g="git"

    # git add
    alias ga="git add"
    alias gaa="git add --all"

    # git branch
    alias gb="git branch"

    # git checkout
    alias gco="git checkout"

    # git commit
    alias gc="git commit"
    alias gc!="git commit --amend"
    alias gcn!="git commit --amend --no-edit"
    alias gca="git commit -a"
    alias gca!="git commit -a --amend"
    alias gcan!="git commit -a --amend --no-edit"
    alias gcm="git commit -m"
    alias gcm!="git commit --amend -m"
    alias gcmn!="git commit --amend --no-edit -m"
    alias gcma="git commit -a -m"
    alias gcma!="git commit -a --amend -m"
    alias gcman!="git commit -a --amend --no-edit -m"

    # git init
    alias gi="git init"

    # git pull
    alias gl="git pull"

    # git push
    alias gp="git push"
    alias gp!="git push --force"
    alias gpr="git push --rebase"

    # git rm
    alias grm="git rm"
    alias grmc="git rm --cached"

    # git restore
    alias grs='git restore'

    # git show
    alias gsh='git show'

    # git status
    alias gst="git status"
    alias gss="git status -s"
fi
