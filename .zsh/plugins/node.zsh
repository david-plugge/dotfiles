# fnm
export PATH="$HOME/.local/share/fnm:$PATH"
eval "$(fnm env)"

# pnpm
export PNPM_HOME="$HOME/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"

# delete all node_modules
alias delete-nodemodules="find . -name 'node_modules' -type d -prune -print -exec rm -rf '{}' \;"

# pnpm
alias p="pnpm"
alias px="pnpx"
alias pd="pnpm dev"
alias pb="pnpm build"
alias pi="pnpm install"
alias pa="pnpm add"
alias pad="pnpm add --save-dev"
alias pu="pnpm up"

# bun
alias b="bun"
alias bx="bunx"
alias bd="bun --bun run dev"
alias bb="bun run build"
alias bi="bun install"
alias ba="bun add"
alias bad="bun add --dev"
alias bu="bun update"
alias bul="bun update --prefer-latest"

# vite
alias vite-node-ssr="vite-node --options.transformMode.ssr='/.*/'"
